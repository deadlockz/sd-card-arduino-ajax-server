HanRun Ethernet Shield with SPI SS for SD-card on Pin 4 (Arduino Uno) as
Webserver. DHCP Client, LCD 16x2, relais (5) and LDR (A2) sensor added. 
Handle htm, md, png, css, ico and js via SD read. Switch relais via web,
LDR sensor read via ajax.

It is a basically project for Internet-of-Things (IoT) with control
and GUI AJAX display stuff, I think.

# Images

## The Device

![device](device.jpg)

## The LAN Shield

Many of these Shields have SD card access trouble.

![The LAN Shield](LAN-board.jpg)

## The default Website

- link to switch relais
- read alls analog in (not A0, not A1, because they are for LCD)
- link to SD Card examples
- Refresh by it self

![default website](inputs-and-output.jpg)

## The Test Website

test.html has inline request to `favicon.ico`, `aclick.js`, `logo.png`, `style.css`

![test SD card website](test-css-js-img-ico.jpg)

## Ajax Website

- has a svg in its html code
- javascript make continous `/analog2` request
- respose is interpreted as degreen into svg (not as kmh value)

![ajax response LDR value and used as rotation degree in SVG](ajax-ldr-value-is-degree.mp4)

# hint

the md File is displayed text only and not html converted by arduino!