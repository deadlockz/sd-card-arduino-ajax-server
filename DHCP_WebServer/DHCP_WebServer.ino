// Ethernet shield attached to pins 10(CS), 11, 12, 13 // = SPI
// pin4 vor SD Card (ss)

#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>
#include <LiquidCrystal.h>

#define RELAISPIN 5
#define LDRANALOG 2

File myFile;
String req;
bool relais;
EthernetClient client;
LiquidCrystal lcd(A0, A1, 9, 8, 7, 6); // Analog 0 and 1 used for Display!!!!

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 200); // if DHCP fails

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);

void setup() {
  pinMode(RELAISPIN, OUTPUT);
  digitalWrite(RELAISPIN, HIGH); // high is off!
  relais = false;
  
  lcd.begin(16, 2);

  if (!SD.begin(4)) { // SS on pin 4
    lcd.print(F("SD failed!"));
    return;
  }
  
  // Trying DHCP
  if (Ethernet.begin(mac) == 0) {
    // initialize the Ethernet device not using DHCP:
    Ethernet.begin(mac, ip);
  }
  ip = Ethernet.localIP();
  
  server.begin();
  lcd.print(Ethernet.localIP());
}

bool handlereq (const __FlashStringHelper* ty, const __FlashStringHelper* mim, int s) {
  int sindex = req.indexOf(ty, 0);
  if (sindex > 4) {
    myFile = SD.open(req.substring(4, sindex+s).c_str());
    client.println(F("HTTP/1.1 200 OK"));
    client.println(mim);
    client.println();
    while (myFile.available()) {
      client.print((char) myFile.read());
    }
    myFile.close();
    return true;
  }
  return false; 
}

void loop() {
  client = server.available();
  if (!client == false) {
    req = client.readStringUntil('\r');
    lcd.setCursor(0, 1);
    lcd.print(req.c_str());
    
    client.flush();
    client.println(F("HTTP/1.1 200 OK"));

    bool res = handlereq(F(".md "), F("Content-Type: text/plain"), 3);
    if (res) {
      client.stop();
      return;
    }
    res = handlereq(F(".png "), F("Content-Type: image/png"), 4);
    if (res) {
      client.stop();
      return;
    }
    res = handlereq(F(".htm "), F("Content-Type: text/html"), 4);
    if (res) {
      client.stop();
      return;
    }
    res = handlereq(F(".css "), F("Content-Type: text/css"), 4);
    if (res) {
      client.stop();
      return;
    }
    res = handlereq(F(".ico "), F("Content-Type: image/vnd.microsoft.icon"), 4);
    if (res) {
      client.stop();
      return;
    }
    res = handlereq(F(".js "), F("Content-Type: application/javascript"), 3);
    if (res) {
      client.stop();
      return;
      
    } else {
      int sindex = req.indexOf("GET /relaison ", 0);
      if (sindex == 0) {
        relais = true;
        digitalWrite(RELAISPIN, LOW);
      }
      sindex = req.indexOf("GET /relaisoff ", 0);
      if (sindex == 0) {
        relais = false;
        digitalWrite(RELAISPIN, HIGH);
      }
      sindex = req.indexOf("GET /analog2 ", 0);
      if (sindex == 0) {
        client.println(F("Content-Type: text/plain"));
        client.println();
        int sensorReading = analogRead(LDRANALOG);
        client.print(sensorReading);
        client.stop();
        
      } else {
            
        client.println(F("Content-Type: text/html"));
        client.println(F("Connection: close"));  // the connection will be closed after completion of the response
        client.println(F("Refresh: 3"));  // refresh the page automatically every 5 sec
        client.println();
        client.print(F("<!DOCTYPE html>\n<html><head><title>Zeug</title><meta charset='utf-8' /></head>"));
        client.print(F("<body bgcolor='white' text='black'><h1>Zeug</h1>"));
        client.print(F("<pre>\n"));
        client.print(F("<a href='/'>refresh site</a>\n\n"));
        // output the value of each free analog input pin
        for (int analogChannel = 2; analogChannel < 6; analogChannel++) {
          int sensorReading = analogRead(analogChannel);
          client.print(F("analog input "));
          client.print(analogChannel);
          client.print(F(" is "));
          client.println(sensorReading);
        }
        client.print(F("relais is "));
        if (relais) {
          client.println(F("<a href='/relaisoff'>on</a>"));
        } else {
          client.println(F("<a href='/relaison'>off</a>"));
        }
        client.print(F("</pre>"));
        client.print(F("<a href='/example.md'>example.md</a> "));
        client.print(F("<a href='/test.htm' >test.htm</a> "));
        client.print(F("<a href='/ajax.htm'>ajax.htm</a>"));
        client.println(F("</body></html>"));
        client.stop();
      }
    }
  }
}

